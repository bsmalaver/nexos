package com.prueba.nexos.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

@Entity
@ToString
@Table(name = "cargo")
public class DTOIntCargo {

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Getter @Setter @Column(name = "idcargo")
     private Integer id;

     @Getter @Setter @Column(name = "descripcion")
     private String descripcion;

}
