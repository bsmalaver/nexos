package com.prueba.nexos.model.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.Date;

@Entity
@ToString
@Table(name = "usuario")
public class DTOIntUsuario {

     @Getter @Setter
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Id @Column(name = "idusuario")
     private int id;

     @Getter @Setter @Column(name = "nombre")
     private String nombre;

     @Getter @Setter @Column(name = "edad")
     private String edad;

     @ManyToOne(fetch = FetchType.LAZY)
     @JoinColumn(name = "idcargo", insertable = false, updatable = false)
     @Getter @Setter
     private DTOIntCargo cargo;

     @Getter @Setter @Column(name = "idcargo")
     private Integer idCargo;

     @Getter @Setter @Column(name = "fechaingreso")
     private Date fechaDeIngresoACompañia;

}
