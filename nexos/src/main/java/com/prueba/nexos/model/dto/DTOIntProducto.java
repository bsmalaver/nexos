package com.prueba.nexos.model.dto;


import com.prueba.nexos.model.validations.IValidateProduct;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import org.springframework.lang.NonNullFields;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@ToString
@Table(name = "producto")
public class DTOIntProducto {

     @Id
     @GeneratedValue(strategy = GenerationType.IDENTITY)
     @Getter @Setter @Column(name = "idproducto")
     private int id;

     @NonNull
     @Getter @Setter @Column(name = "nombreproducto")
     private String nombreProducto;

     @Getter @Setter @Column(name = "descripcionproducto")
     private String descripcionProducto;

     @Getter @Setter @Column(name = "cantidad")
     private Integer cantidad;

     @Getter @Setter @Column(name = "fechaingreso")
     private Date fechaDeIngreso;

     @Getter @Setter
     @ManyToOne(fetch = FetchType.LAZY)
     @JoinColumn(name = "idusuario", insertable = false, updatable = false)
     private DTOIntUsuario usuario;

     @Getter @Setter
     @ManyToOne(fetch = FetchType.LAZY)
     @JoinColumn(name = "usuarioactualizo", insertable = false, updatable = false)
     private DTOIntUsuario usuarioActualizo;

     @Getter @Setter @Column(name = "idusuario" )
     private Integer idUsuario;

     @Getter @Setter @Column(name = "usuarioactualizo")
     private Integer idUsuarioActualizo;

     @Getter @Setter @Column(name = "fechaactualizacion")
     private Date fechaDeActualizacion;

}
