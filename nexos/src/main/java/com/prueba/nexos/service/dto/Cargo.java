package com.prueba.nexos.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Cargo implements Serializable {

     @Getter
     @Setter
     private Integer id;
     @Getter
     @Setter
     private String descripcion;
}
