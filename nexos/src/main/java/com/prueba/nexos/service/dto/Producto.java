package com.prueba.nexos.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;

import lombok.NonNull;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Producto implements Serializable {

     @Getter
     @Setter
     private int id;
     @Getter
     @Setter
     @NonNull
     private String nombreProducto;
     @Getter
     @Setter
     private String descripcionProducto;
     @Getter
     @Setter
     private Integer cantidad;
     @Getter
     @Setter
     private Date fechaDeIngreso;
     @Getter
     @Setter
     private Usuario usuario;
     @Getter
     @Setter
     private Usuario usuarioActualizo;
     @Getter
     @Setter
     private Integer idUsuario;
     @Getter @Setter
     private Integer idUsuarioActualizo;
}
