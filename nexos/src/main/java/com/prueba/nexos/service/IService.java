package com.prueba.nexos.service;

import com.prueba.nexos.model.dto.DTOIntCargo;
import com.prueba.nexos.model.dto.DTOIntProducto;
import com.prueba.nexos.model.dto.DTOIntUsuario;
import com.prueba.nexos.service.dto.Producto;
import com.prueba.nexos.service.dto.Usuario;

import java.util.List;

public interface IService {

     void createProduct(DTOIntProducto dtoIntProducto);

     Producto getProduct (String nombreProducto);

     List<Producto> listProducts ();


     void updateProduct(DTOIntProducto dtoIntProducto, String product);

     void removeProduct(String nombreProducto, String usuario);

     void createCargo(DTOIntCargo dtoIntCargo);

     List<Usuario> listUsers();

     void createUser(DTOIntUsuario dtoIntUsuario);

}
