package com.prueba.nexos.service.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
@JsonInclude(JsonInclude.Include.NON_NULL)
public class  Usuario implements Serializable {

     @Getter @Setter
     private int id;
     @Getter @Setter
     private String nombre;
     @Getter @Setter
     private String edad;
     @Getter @Setter
     private Cargo cargo;
     @Getter @Setter
     private Integer idCargo;
     @Getter @Setter
     private Date fechaDeIngresoACompañia;

}
