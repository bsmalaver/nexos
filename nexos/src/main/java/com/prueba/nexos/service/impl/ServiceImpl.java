package com.prueba.nexos.service.impl;

import com.prueba.nexos.model.dto.DTOIntCargo;
import com.prueba.nexos.model.dto.DTOIntProducto;
import com.prueba.nexos.model.dto.DTOIntUsuario;
import com.prueba.nexos.repository.IRepositoryPosition;
import com.prueba.nexos.repository.IRepositoryProduct;
import com.prueba.nexos.repository.IRepositoryUser;
import com.prueba.nexos.service.IService;
import com.prueba.nexos.service.dto.Producto;
import com.prueba.nexos.service.dto.Usuario;
import com.prueba.nexos.service.mapper.IServiceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ServiceImpl implements IService {

     @Autowired
     IRepositoryProduct iRepositoryProduct;

     @Autowired
     IRepositoryPosition iRepositoryPosition;

     @Autowired
     IRepositoryUser iRepositoryUser;

     @Resource(name = "serviceMapper")
     IServiceMapper iServiceMapper;


     @Override
     public void createProduct(DTOIntProducto dtoIntProducto){
          iRepositoryProduct.save(dtoIntProducto);
     }

     @Override
     public Producto getProduct(String nombreProducto) {
          return iServiceMapper.mapOutProduct(iRepositoryProduct.findAllByNombreProducto(nombreProducto));
     }

     @Override
     public List<Producto> listProducts() {
          return iServiceMapper.mapOutListProduct(iRepositoryProduct.findAll());
     }

     @Override
     public void updateProduct(DTOIntProducto dtoIntProducto, String product) {
          DTOIntProducto producto = iRepositoryProduct.findByNombreProducto(product);
          producto.setNombreProducto(dtoIntProducto.getNombreProducto());
          producto.setCantidad(dtoIntProducto.getCantidad());
          producto.setFechaDeIngreso(dtoIntProducto.getFechaDeIngreso());
          producto.setDescripcionProducto(dtoIntProducto.getDescripcionProducto());
          producto.setFechaDeActualizacion(dtoIntProducto.getFechaDeActualizacion());
          producto.setIdUsuarioActualizo(dtoIntProducto.getIdUsuarioActualizo());
          producto.setUsuario(dtoIntProducto.getUsuario());
          iRepositoryProduct.save(producto);
     }

     @Override
     public void removeProduct(String nombreProducto, String usuario) {
          DTOIntProducto dtoIntProducto = iRepositoryProduct.findByNombreProducto(nombreProducto);
          if (dtoIntProducto.getUsuario().getNombre().equalsIgnoreCase(usuario)) {
               iRepositoryProduct.delete(dtoIntProducto);
          }
     }

     @Override
     public List<Usuario> listUsers() {
          return iServiceMapper.mapOutUsuario(iRepositoryUser.findAll());
     }

     @Override
     public void createUser(DTOIntUsuario dtoIntUsuario) {
          iRepositoryUser.save(dtoIntUsuario);
     }

     @Override
     public void createCargo(DTOIntCargo dtoIntCargo) {
          iRepositoryPosition.save(dtoIntCargo);
     }


}
