package com.prueba.nexos.service.mapper;

import com.prueba.nexos.model.dto.DTOIntCargo;
import com.prueba.nexos.model.dto.DTOIntProducto;
import com.prueba.nexos.model.dto.DTOIntUsuario;
import com.prueba.nexos.service.dto.Cargo;
import com.prueba.nexos.service.dto.Producto;
import com.prueba.nexos.service.dto.Usuario;

import java.util.List;

public interface IServiceMapper {

     DTOIntProducto mapInProduct(Producto producto);
     DTOIntProducto mapInUpdateProduct(Producto producto);
     List<Producto> mapOutListProduct(List<DTOIntProducto> dtoIntProducto);
     Producto mapOutProduct(DTOIntProducto dtoIntProducto);
     DTOIntCargo mapInPosition(Cargo cargo);
     DTOIntUsuario mapIntUser(Usuario usuario);
     List<Usuario> mapOutUsuario(List<DTOIntUsuario> dtoIntUsuario);
}
