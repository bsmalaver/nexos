package com.prueba.nexos.service.mapper.impl;

import com.prueba.nexos.model.dto.DTOIntCargo;
import com.prueba.nexos.model.dto.DTOIntProducto;
import com.prueba.nexos.model.dto.DTOIntUsuario;
import com.prueba.nexos.service.dto.Cargo;
import com.prueba.nexos.service.dto.Producto;
import com.prueba.nexos.service.dto.Usuario;
import com.prueba.nexos.service.mapper.IServiceMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component(value = "serviceMapper")
public class ServiceMapper implements IServiceMapper {

     private static final Logger logger = LoggerFactory.getLogger(ServiceMapper.class);
     private static final String ERROR = "Ha ocurrido un error inesperado: ";

     @Override
     public DTOIntProducto mapInProduct(Producto producto) {
          DTOIntProducto dtoIntProducto = new DTOIntProducto();
          try {
               dtoIntProducto.setId(producto.getId());
               dtoIntProducto.setNombreProducto(producto.getNombreProducto());
               dtoIntProducto.setDescripcionProducto(producto.getDescripcionProducto());
               dtoIntProducto.setCantidad(producto.getCantidad());
               dtoIntProducto.setIdUsuario(producto.getIdUsuario());
               dtoIntProducto.setFechaDeIngreso(producto.getFechaDeIngreso());
          } catch (Exception n) {
               logger.error(ERROR, n);
          }
          return dtoIntProducto;
     }

     @Override
     public DTOIntProducto mapInUpdateProduct(Producto producto) {
          DTOIntProducto dtoIntProducto = new DTOIntProducto();
          dtoIntProducto.setId(producto.getId());
          dtoIntProducto.setNombreProducto(producto.getNombreProducto());
          dtoIntProducto.setDescripcionProducto(producto.getDescripcionProducto());
          dtoIntProducto.setCantidad(producto.getCantidad());
          dtoIntProducto.setFechaDeIngreso(producto.getFechaDeIngreso());
          dtoIntProducto.setIdUsuario(producto.getIdUsuario());
          dtoIntProducto.setIdUsuarioActualizo(producto.getIdUsuarioActualizo());
          dtoIntProducto.setFechaDeActualizacion(new Date());
          return dtoIntProducto;
     }

     @Override
     public List<Producto> mapOutListProduct(List<DTOIntProducto> dtoIntProducto) {
          List<Producto> listProducts = new ArrayList<>();
          for (DTOIntProducto pr : dtoIntProducto) {
               Producto producto = new Producto();
               producto.setId(pr.getId());
               producto.setNombreProducto(pr.getNombreProducto());
               producto.setCantidad(pr.getCantidad());
               producto.setDescripcionProducto(pr.getDescripcionProducto());
               producto.setFechaDeIngreso(pr.getFechaDeIngreso());
               if (pr.getUsuario() != null) {
                    Usuario usuario = new Usuario();
                    usuario.setId(pr.getUsuario().getId());
                    usuario.setNombre(pr.getUsuario().getNombre());
                    usuario.setEdad(pr.getUsuario().getEdad());
                    if (pr.getUsuario().getCargo() != null) {
                         Cargo cargo = new Cargo();
                         cargo.setId(pr.getUsuario().getCargo().getId());
                         cargo.setDescripcion(pr.getUsuario().getCargo().getDescripcion());
                         usuario.setCargo(cargo);
                    }
                    usuario.setFechaDeIngresoACompañia(pr.getUsuario().getFechaDeIngresoACompañia());
                    producto.setUsuario(usuario);
               }
               if (pr.getUsuarioActualizo() != null) {
                    Usuario usuarioActualizo = new Usuario();
                    usuarioActualizo.setId(pr.getId());
                    usuarioActualizo.setNombre(pr.getUsuarioActualizo().getNombre());
                    usuarioActualizo.setEdad(pr.getUsuarioActualizo().getEdad());
                    if (pr.getUsuarioActualizo().getCargo() != null) {
                         Cargo cargo = new Cargo();
                         cargo.setId(pr.getUsuarioActualizo().getCargo().getId());
                         cargo.setDescripcion(pr.getUsuarioActualizo().getCargo().getDescripcion());
                         usuarioActualizo.setCargo(cargo);
                    }
                    usuarioActualizo.setFechaDeIngresoACompañia(pr.getUsuario().getFechaDeIngresoACompañia());
                    producto.setUsuarioActualizo(usuarioActualizo);
               }
               listProducts.add(producto);
          }
          return listProducts;
     }

     @Override
     public Producto mapOutProduct(DTOIntProducto dtoIntProducto) {

               Producto producto = new Producto();
               producto.setId(dtoIntProducto.getId());
               producto.setNombreProducto(dtoIntProducto.getNombreProducto());
               producto.setCantidad(dtoIntProducto.getCantidad());
               producto.setDescripcionProducto(dtoIntProducto.getDescripcionProducto());
               producto.setFechaDeIngreso(dtoIntProducto.getFechaDeIngreso());
               if (dtoIntProducto.getUsuario() != null) {
                    Usuario usuario = new Usuario();
                    usuario.setId(dtoIntProducto.getUsuario().getId());
                    usuario.setNombre(dtoIntProducto.getUsuario().getNombre());
                    usuario.setEdad(dtoIntProducto.getUsuario().getEdad());
                    if (dtoIntProducto.getUsuario().getCargo() != null) {
                         Cargo cargo = new Cargo();
                         cargo.setId(dtoIntProducto.getUsuario().getCargo().getId());
                         cargo.setDescripcion(dtoIntProducto.getUsuario().getCargo().getDescripcion());
                         usuario.setCargo(cargo);
                    }
                    usuario.setFechaDeIngresoACompañia(dtoIntProducto.getUsuario().getFechaDeIngresoACompañia());
                    producto.setUsuario(usuario);
               }
               if (dtoIntProducto.getUsuarioActualizo() != null) {
                    Usuario usuarioActualizo = new Usuario();
                    usuarioActualizo.setId(dtoIntProducto.getId());
                    usuarioActualizo.setNombre(dtoIntProducto.getUsuarioActualizo().getNombre());
                    usuarioActualizo.setEdad(dtoIntProducto.getUsuarioActualizo().getEdad());
                    if (dtoIntProducto.getUsuarioActualizo().getCargo() != null) {
                         Cargo cargo = new Cargo();
                         cargo.setId(dtoIntProducto.getUsuarioActualizo().getCargo().getId());
                         cargo.setDescripcion(dtoIntProducto.getUsuarioActualizo().getCargo().getDescripcion());
                         usuarioActualizo.setCargo(cargo);
                    }
                    usuarioActualizo.setFechaDeIngresoACompañia(dtoIntProducto.getUsuario().getFechaDeIngresoACompañia());
                    producto.setUsuarioActualizo(usuarioActualizo);
               }
          return producto;
     }

     @Override
     public DTOIntCargo mapInPosition(Cargo cargo) {
          DTOIntCargo dtoIntCargo = new DTOIntCargo();
          dtoIntCargo.setDescripcion(cargo.getDescripcion());
          return dtoIntCargo;
     }

     @Override
     public DTOIntUsuario mapIntUser(Usuario usuario) {
          DTOIntUsuario dtoIntUsuario = new DTOIntUsuario();
          dtoIntUsuario.setNombre(usuario.getNombre());
          dtoIntUsuario.setEdad(usuario.getEdad());
          dtoIntUsuario.setIdCargo(usuario.getIdCargo());
          dtoIntUsuario.setFechaDeIngresoACompañia(usuario.getFechaDeIngresoACompañia());
          return dtoIntUsuario;
     }

     @Override
     public List<Usuario> mapOutUsuario(List<DTOIntUsuario> dtoIntUsuario) {
          List<Usuario> usuarioList = new ArrayList<>();
          for (DTOIntUsuario intUsuario : dtoIntUsuario) {
               Usuario usuario = new Usuario();
               usuario.setId(intUsuario.getId());
               usuario.setNombre(intUsuario.getNombre());
               usuario.setEdad(intUsuario.getEdad());
               if (intUsuario.getCargo() != null) {
                    Cargo cargo = new Cargo();
                    cargo.setId(intUsuario.getCargo().getId());
                    cargo.setDescripcion(intUsuario.getCargo().getDescripcion());
                    usuario.setCargo(cargo);
               }
               usuario.setFechaDeIngresoACompañia(intUsuario.getFechaDeIngresoACompañia());
               usuarioList.add(usuario);
          }
          return usuarioList;
     }
}
