package com.prueba.nexos.repository;

import com.prueba.nexos.model.dto.DTOIntCargo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRepositoryPosition extends JpaRepository<DTOIntCargo, Integer> {

}
