package com.prueba.nexos.repository;

import com.prueba.nexos.model.dto.DTOIntUsuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IRepositoryUser extends JpaRepository<DTOIntUsuario, Integer> {
}
