package com.prueba.nexos.repository;

import com.prueba.nexos.model.dto.DTOIntProducto;
import com.prueba.nexos.service.dto.Producto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IRepositoryProduct extends JpaRepository<DTOIntProducto, Integer> {

     DTOIntProducto findAllByNombreProducto(String nombreProducto);

     DTOIntProducto findByNombreProducto(String nombreProducto);
}
