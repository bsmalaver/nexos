package com.prueba.nexos.controller;

import com.prueba.nexos.service.IService;
import com.prueba.nexos.service.dto.Cargo;
import com.prueba.nexos.service.dto.Producto;
import com.prueba.nexos.service.dto.Usuario;
import com.prueba.nexos.service.mapper.IServiceMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.net.URI;
import java.util.*;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class Controller {

     @Autowired
     IService iproducto;

     @Resource(name = "serviceMapper")
     IServiceMapper iServiceMapper;

     @PostMapping("/nexos/product")
     @ResponseBody
     public ResponseEntity<Producto> createProduct(@RequestBody Producto producto){
          iproducto.createProduct(iServiceMapper.mapInProduct(producto));
          return ResponseEntity.created(URI.create("/productos")).build();
     }

     @GetMapping("/nexos/products")
     public List<Producto> listProduct(){
          return  iproducto.listProducts();

     }

     @GetMapping("/nexos/product/{product}")
     @ResponseBody
     public Producto getProduct(@PathVariable String product){
         return iproducto.getProduct(product);

     }

     @PatchMapping("/nexos/product/{product}")
     public ResponseEntity<Producto> updateProduct(@RequestBody Producto producto,
                                                  @PathVariable String product){
          iproducto.updateProduct(iServiceMapper.mapInUpdateProduct(producto), product);
          return ResponseEntity.ok().build();
     }

     @DeleteMapping("/nexos/product/{product}")
     public ResponseEntity<String> deleteProduct(@PathVariable String product, @RequestParam String usuario){
          iproducto.removeProduct(product, usuario);
          return ResponseEntity.ok().build();
     }

     @PostMapping("/nexos/usuario")
     public ResponseEntity<Usuario> createUser(@RequestBody Usuario usuario){
          iproducto.createUser(iServiceMapper.mapIntUser(usuario));
          return ResponseEntity.created(URI.create("/usuarios")).build();
     }

     @GetMapping("/nexos/usuarios")
     public List<Usuario> listUsers(){
          return iproducto.listUsers();
     }

     @PostMapping("/nexos/cargos")
     public void createCargo(@RequestBody Cargo cargo){
          iproducto.createCargo(iServiceMapper.mapInPosition(cargo));
     }
}
