package com.prueba.nexos.model.dto;

import org.junit.Assert;
import org.junit.Test;

import java.util.Date;

public class DTOTest {

     @Test
     public void testToStringUsuario() {
          DTOIntUsuario dtoIntUsuario = new DTOIntUsuario();
          dtoIntUsuario.setId(1);
          dtoIntUsuario.setNombre("Brayan");
          dtoIntUsuario.setEdad("21");
          dtoIntUsuario.setIdCargo(1);
          dtoIntUsuario.setFechaDeIngresoACompañia(new Date("2021/12/12"));
          DTOIntCargo dtoIntCargo = new DTOIntCargo();
          dtoIntCargo.setId(1);
          dtoIntCargo.setDescripcion("Soporte");
          dtoIntUsuario.setCargo(dtoIntCargo);
          Assert.assertEquals("DTOIntUsuario(id=1, nombre=Brayan, edad=21, cargo=DTOIntCargo(id=1, descripcion=Soporte), idCargo=1, fechaDeIngresoACompañia=Sun Dec 12 00:00:00 COT 2021)", dtoIntUsuario.toString());

     }

     @Test
     public void testToStringCargo(){
          DTOIntCargo dtoIntCargo = new DTOIntCargo();
          dtoIntCargo.setId(1);
          dtoIntCargo.setDescripcion("Soporte");
          Assert.assertEquals("DTOIntCargo(id=1, descripcion=Soporte)", dtoIntCargo.toString());


     }

     @Test
     public void testToStringProducto(){
          DTOIntProducto dtoIntProducto = new DTOIntProducto();
          dtoIntProducto.setId(1);
          dtoIntProducto.setNombreProducto("XBOX");
          dtoIntProducto.setDescripcionProducto("Consola");
          dtoIntProducto.setCantidad(2);
          dtoIntProducto.setFechaDeIngreso(new Date("2021/12/11"));
          dtoIntProducto.setIdUsuarioActualizo(1);
          dtoIntProducto.setFechaDeActualizacion(new Date("2021/12/23"));
          DTOIntUsuario dtoIntUsuario = new DTOIntUsuario();
          dtoIntUsuario.setId(1);
          dtoIntUsuario.setNombre("Brayan");
          dtoIntUsuario.setEdad("21");
          dtoIntUsuario.setIdCargo(1);
          dtoIntUsuario.setFechaDeIngresoACompañia(new Date("2021/12/12"));
          DTOIntCargo dtoIntCargo = new DTOIntCargo();
          dtoIntCargo.setId(1);
          dtoIntCargo.setDescripcion("Soporte");
          dtoIntUsuario.setCargo(dtoIntCargo);
          dtoIntProducto.setUsuario(dtoIntUsuario);
          DTOIntUsuario dtoIntUsuarioActualizo = new DTOIntUsuario();
          dtoIntUsuarioActualizo.setId(1);
          dtoIntUsuarioActualizo.setNombre("Brayan");
          dtoIntUsuarioActualizo.setEdad("21");
          dtoIntUsuarioActualizo.setIdCargo(1);
          dtoIntUsuarioActualizo.setFechaDeIngresoACompañia(new Date("2021/12/12"));
          DTOIntCargo dtoIntCargoActualizo = new DTOIntCargo();
          dtoIntCargoActualizo.setId(1);
          dtoIntCargoActualizo.setDescripcion("Soporte");
          dtoIntUsuarioActualizo.setCargo(dtoIntCargoActualizo);
          dtoIntProducto.setUsuarioActualizo(dtoIntUsuarioActualizo);
          Assert.assertEquals("DTOIntProducto(id=1, nombreProducto=XBOX, descripcionProducto=Consola, cantidad=2, fechaDeIngreso=Sat Dec 11 00:00:00 COT 2021, usuario=DTOIntUsuario(id=1, nombre=Brayan, edad=21, cargo=DTOIntCargo(id=1, descripcion=Soporte), idCargo=1, fechaDeIngresoACompañia=Sun Dec 12 00:00:00 COT 2021), usuarioActualizo=DTOIntUsuario(id=1, nombre=Brayan, edad=21, cargo=DTOIntCargo(id=1, descripcion=Soporte), idCargo=1, fechaDeIngresoACompañia=Sun Dec 12 00:00:00 COT 2021), idUsuario=null, idUsuarioActualizo=1, fechaDeActualizacion=Thu Dec 23 00:00:00 COT 2021)", dtoIntProducto.toString());
     }

}
