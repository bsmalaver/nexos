package com.prueba.nexos;

import com.prueba.nexos.model.dto.DTOIntCargo;
import com.prueba.nexos.model.dto.DTOIntProducto;
import com.prueba.nexos.model.dto.DTOIntUsuario;
import com.prueba.nexos.service.dto.Cargo;
import com.prueba.nexos.service.dto.Producto;
import com.prueba.nexos.service.dto.Usuario;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DummyMock {

     public List<DTOIntProducto> getOutProduct() {
          List<DTOIntProducto> productoList = new ArrayList<>();
          DTOIntProducto dtoIntProducto = new DTOIntProducto();
          dtoIntProducto.setId(1);
          dtoIntProducto.setNombreProducto("XBOX");
          dtoIntProducto.setDescripcionProducto("Consola de videojuegos");
          dtoIntProducto.setCantidad(8);
          dtoIntProducto.setIdUsuario(4);
          dtoIntProducto.setFechaDeIngreso(new Date("2021/12/12"));
          DTOIntUsuario dtoIntUsuario = new DTOIntUsuario();
          dtoIntUsuario.setId(1);
          dtoIntUsuario.setNombre("Brayan");
          DTOIntCargo dtoIntCargo = new DTOIntCargo();
          dtoIntCargo.setId(1);
          dtoIntCargo.setDescripcion("Soporte");
          dtoIntUsuario.setCargo(dtoIntCargo);
          dtoIntUsuario.setEdad("21");
          dtoIntUsuario.setFechaDeIngresoACompañia(new Date("2021/11/05"));
          dtoIntProducto.setUsuario(dtoIntUsuario);
          DTOIntUsuario usuarioActualizo = new DTOIntUsuario();
          usuarioActualizo.setId(1);
          usuarioActualizo.setNombre("Brayan");
          DTOIntCargo cargoActualizo = new DTOIntCargo();
          cargoActualizo.setId(1);
          cargoActualizo.setDescripcion("Soporte");
          usuarioActualizo.setCargo(cargoActualizo);
          usuarioActualizo.setEdad("21");
          usuarioActualizo.setFechaDeIngresoACompañia(new Date("2021/11/05"));
          dtoIntProducto.setUsuarioActualizo(usuarioActualizo);
          productoList.add(dtoIntProducto);
          return productoList;
     }

     public DTOIntProducto getOutProductDTO() {
          DTOIntProducto dtoIntProducto = new DTOIntProducto();
          dtoIntProducto.setId(1);
          dtoIntProducto.setNombreProducto("XBOX");
          dtoIntProducto.setDescripcionProducto("Consola de videojuegos");
          dtoIntProducto.setCantidad(8);
          dtoIntProducto.setIdUsuario(4);
          dtoIntProducto.setFechaDeIngreso(new Date("2021/12/12"));
          DTOIntUsuario dtoIntUsuario = new DTOIntUsuario();
          dtoIntUsuario.setId(1);
          dtoIntUsuario.setNombre("Brayan");
          DTOIntCargo dtoIntCargo = new DTOIntCargo();
          dtoIntCargo.setId(1);
          dtoIntCargo.setDescripcion("Soporte");
          dtoIntUsuario.setCargo(dtoIntCargo);
          dtoIntUsuario.setEdad("21");
          dtoIntUsuario.setFechaDeIngresoACompañia(new Date("2021/11/05"));
          dtoIntProducto.setUsuario(dtoIntUsuario);
          DTOIntUsuario usuarioActualizo = new DTOIntUsuario();
          usuarioActualizo.setId(1);
          usuarioActualizo.setNombre("Brayan");
          DTOIntCargo cargoActualizo = new DTOIntCargo();
          cargoActualizo.setId(1);
          cargoActualizo.setDescripcion("Soporte");
          usuarioActualizo.setCargo(cargoActualizo);
          usuarioActualizo.setEdad("21");
          usuarioActualizo.setFechaDeIngresoACompañia(new Date("2021/11/05"));
          dtoIntProducto.setUsuarioActualizo(usuarioActualizo);
          return dtoIntProducto;
     }


     public List<Producto> getOutListProduct() {
          List<Producto> productoList = new ArrayList<>();
          Producto producto = new Producto();
          producto.setId(1);
          producto.setNombreProducto("XBOX");
          producto.setDescripcionProducto("Consola de videojuegos");
          producto.setCantidad(8);
          producto.setIdUsuario(4);
          producto.setFechaDeIngreso(new Date("2021/12/12"));
          Usuario usuario = new Usuario();
          usuario.setId(1);
          usuario.setNombre("Brayan");
          Cargo cargo = new Cargo();
          cargo.setId(1);
          cargo.setDescripcion("Soporte");
          usuario.setCargo(cargo);
          usuario.setEdad("21");
          usuario.setFechaDeIngresoACompañia(new Date("2021/11/05"));
          producto.setUsuario(usuario);
          Usuario usuarioActualizo = new Usuario();
          usuarioActualizo.setId(1);
          usuarioActualizo.setNombre("Brayan");
          Cargo cargoActualizo = new Cargo();
          cargoActualizo.setId(1);
          cargoActualizo.setDescripcion("Soporte");
          usuarioActualizo.setCargo(cargoActualizo);
          usuarioActualizo.setEdad("21");
          usuarioActualizo.setFechaDeIngresoACompañia(new Date("2021/11/05"));
          producto.setUsuarioActualizo(usuarioActualizo);
          productoList.add(producto);
          return productoList;
     }

     public Producto getMapOutProduct() {
          Producto producto = new Producto();
          producto.setId(1);
          producto.setNombreProducto("XBOX");
          producto.setDescripcionProducto("Consola de videojuegos");
          producto.setCantidad(8);
          producto.setIdUsuario(4);
          producto.setFechaDeIngreso(new Date("2021/12/12"));
          Usuario usuario = new Usuario();
          usuario.setId(1);
          usuario.setNombre("Brayan");
          Cargo cargo = new Cargo();
          cargo.setId(1);
          cargo.setDescripcion("Soporte");
          usuario.setCargo(cargo);
          usuario.setEdad("21");
          usuario.setFechaDeIngresoACompañia(new Date("2021/11/05"));
          producto.setUsuario(usuario);
          Usuario usuarioActualizo = new Usuario();
          usuarioActualizo.setId(1);
          usuarioActualizo.setNombre("Brayan");
          Cargo cargoActualizo = new Cargo();
          cargoActualizo.setId(1);
          cargoActualizo.setDescripcion("Soporte");
          usuarioActualizo.setCargo(cargoActualizo);
          usuarioActualizo.setEdad("21");
          usuarioActualizo.setFechaDeIngresoACompañia(new Date("2021/11/05"));
          producto.setUsuarioActualizo(usuarioActualizo);
          return producto;
     }

     public DTOIntProducto getCreateInProduct(){
          DTOIntProducto dtoIntProducto = new DTOIntProducto();
          dtoIntProducto.setNombreProducto("XBOX");
          dtoIntProducto.setCantidad(2);
          dtoIntProducto.setIdUsuario(2);
          dtoIntProducto.setFechaDeIngreso(new Date("2021/12/11"));
          dtoIntProducto.setDescripcionProducto("Consola");
          return dtoIntProducto;
     }

     public DTOIntProducto getProduct(){
          DTOIntProducto dtoIntProducto = new DTOIntProducto();
          dtoIntProducto.setNombreProducto("XBOX");
          dtoIntProducto.setCantidad(2);
          dtoIntProducto.setIdUsuario(2);
          dtoIntProducto.setFechaDeIngreso(new Date("2021/12/11"));
          dtoIntProducto.setDescripcionProducto("Consola");
          return dtoIntProducto;
     }

     public DTOIntProducto getDeleteProduct(){
          DTOIntProducto dtoIntProducto = new DTOIntProducto();
          dtoIntProducto.setNombreProducto("XBOX");
          dtoIntProducto.setCantidad(2);
          dtoIntProducto.setIdUsuario(2);
          dtoIntProducto.setFechaDeIngreso(new Date("2021/12/11"));
          dtoIntProducto.setDescripcionProducto("Consola");
          DTOIntUsuario dtoIntUsuario = new DTOIntUsuario();
          dtoIntUsuario.setId(1);
          dtoIntUsuario.setNombre("Brayan");
          DTOIntCargo dtoIntCargo = new DTOIntCargo();
          dtoIntCargo.setId(1);
          dtoIntCargo.setDescripcion("Soporte");
          dtoIntUsuario.setCargo(dtoIntCargo);
          dtoIntUsuario.setEdad("21");
          dtoIntUsuario.setFechaDeIngresoACompañia(new Date("2021/11/05"));
          dtoIntProducto.setUsuario(dtoIntUsuario);
          return dtoIntProducto;
     }

     public Producto getInProduct() {
          Producto producto = new Producto();
          producto.setId(1);
          producto.setNombreProducto("XBOX");
          producto.setDescripcionProducto("Consola de videojuegos");
          producto.setCantidad(8);
          producto.setIdUsuario(4);
          producto.setFechaDeIngreso(new Date("2021/12/12"));

          return producto;
     }

     public Producto getInProductNull() {
          Producto producto = new Producto();
          producto.setDescripcionProducto("Consola de videojuegos");
          producto.setCantidad(8);
          producto.setIdUsuario(4);
          producto.setFechaDeIngreso(new Date("2021/12/12"));

          return producto;
     }

     public Producto getInProductUpdate() {
          Producto producto = new Producto();
          producto.setId(1);
          producto.setNombreProducto("XBOX");
          producto.setDescripcionProducto("Consola de videojuegos");
          producto.setCantidad(8);
          producto.setIdUsuario(4);
          producto.setIdUsuarioActualizo(2);
          producto.setFechaDeIngreso(new Date("2021/12/12"));

          return producto;
     }

     public Cargo getMapInPosition(){
          Cargo cargo = new Cargo();
          cargo.setDescripcion("Soporte");
          return cargo;
     }

     public DTOIntCargo getCreatePosition(){
          DTOIntCargo cargo = new DTOIntCargo();
          cargo.setDescripcion("Soporte");
          return cargo;
     }

     public Usuario getMapInUser(){
          Usuario usuario = new Usuario();
          usuario.setNombre("Brayan");
          usuario.setEdad("21");
          usuario.setFechaDeIngresoACompañia(new Date("2021/12/18"));
          usuario.setIdCargo(2);
          return usuario;
     }

     public DTOIntUsuario getCreateUser(){
          DTOIntUsuario dtoIntUsuario = new DTOIntUsuario();
          dtoIntUsuario.setNombre("Brayan");
          dtoIntUsuario.setEdad("21");
          dtoIntUsuario.setFechaDeIngresoACompañia(new Date("2021/12/18"));
          dtoIntUsuario.setIdCargo(2);
          return dtoIntUsuario;
     }

     public List<DTOIntUsuario> getListUsers(){
          List<DTOIntUsuario> dtoIntUsuarioList = new ArrayList<>();
          DTOIntUsuario dtoIntUsuario = new DTOIntUsuario();
          dtoIntUsuario.setId(1);
          dtoIntUsuario.setNombre("Brayan");
          DTOIntCargo dtoIntCargo = new DTOIntCargo();
          dtoIntCargo.setId(1);
          dtoIntCargo.setDescripcion("Soporte");
          dtoIntUsuario.setCargo(dtoIntCargo);
          dtoIntUsuario.setEdad("21");
          dtoIntUsuario.setFechaDeIngresoACompañia(new Date("2021/11/05"));
          dtoIntUsuarioList.add(dtoIntUsuario);
          return dtoIntUsuarioList;
     }

     public List<Usuario> getOutListUsers(){
          List<Usuario> usuarioList = new ArrayList<>();
          Usuario usuario = new Usuario();
          usuario.setId(1);
          usuario.setNombre("Brayan");
          Cargo cargo = new Cargo();
          cargo.setId(1);
          cargo.setDescripcion("Soporte");
          usuario.setCargo(cargo);
          usuario.setEdad("21");
          usuario.setFechaDeIngresoACompañia(new Date("2021/11/05"));
          usuarioList.add(usuario);
          return usuarioList;
     }
}
