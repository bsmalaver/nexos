package com.prueba.nexos.service.mapper;

import com.prueba.nexos.DummyMock;
import com.prueba.nexos.model.dto.DTOIntCargo;
import com.prueba.nexos.model.dto.DTOIntProducto;
import com.prueba.nexos.model.dto.DTOIntUsuario;
import com.prueba.nexos.service.dto.Cargo;
import com.prueba.nexos.service.dto.Producto;
import com.prueba.nexos.service.dto.Usuario;
import com.prueba.nexos.service.mapper.impl.ServiceMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Date;
import java.util.List;

@SpringBootTest
public class ServiceMapperTest {

     private IServiceMapper iServiceMapper;
     private DummyMock dummyMock;

     @Before
     public void setUp() {
          dummyMock = new DummyMock();
          iServiceMapper = new ServiceMapper();
     }

     @Test
     public void testMapInCreateProduct() {
          Producto producto = dummyMock.getInProduct();
          DTOIntProducto dtoIntProducto = iServiceMapper.mapInProduct(producto);
          Assert.assertNotNull(dtoIntProducto);
          Assert.assertEquals(dtoIntProducto.getId(), producto.getId());
          Assert.assertEquals(dtoIntProducto.getNombreProducto(), producto.getNombreProducto());
          Assert.assertEquals(dtoIntProducto.getDescripcionProducto(), producto.getDescripcionProducto());
          Assert.assertEquals(dtoIntProducto.getCantidad(), producto.getCantidad());
          Assert.assertEquals(dtoIntProducto.getFechaDeIngreso(), producto.getFechaDeIngreso());
          Assert.assertEquals(dtoIntProducto.getIdUsuario(), producto.getIdUsuario());
     }

     @Test
     public void testMapInCreateProductNullTryCatch() {
          Producto producto = dummyMock.getInProductNull();
          DTOIntProducto dtoIntProducto = iServiceMapper.mapInProduct(producto);
          Assert.assertNotNull(dtoIntProducto);
     }

     @Test
     public void testMapInUpdateProduct() {
          Producto producto = dummyMock.getInProductUpdate();
          DTOIntProducto dtoIntProducto = iServiceMapper.mapInUpdateProduct(producto);
          Assert.assertNotNull(dtoIntProducto);
          Assert.assertEquals(dtoIntProducto.getId(), producto.getId());
          Assert.assertEquals(dtoIntProducto.getNombreProducto(), producto.getNombreProducto());
          Assert.assertEquals(dtoIntProducto.getDescripcionProducto(), producto.getDescripcionProducto());
          Assert.assertEquals(dtoIntProducto.getCantidad(), producto.getCantidad());
          Assert.assertEquals(dtoIntProducto.getFechaDeIngreso(), producto.getFechaDeIngreso());
          Assert.assertEquals(dtoIntProducto.getIdUsuario(), producto.getIdUsuario());
          Assert.assertEquals(dtoIntProducto.getIdUsuarioActualizo(), producto.getIdUsuarioActualizo());
          Assert.assertEquals(dtoIntProducto.getFechaDeActualizacion(), new Date());
     }

     @Test
     public void testMapOutListProduct(){
          List<DTOIntProducto> dtoIntProduct = dummyMock.getOutProduct();
          List<Producto> product = iServiceMapper.mapOutListProduct(dtoIntProduct);
          Assert.assertNotNull(product.get(0));
          Assert.assertEquals(product.get(0).getId(), dtoIntProduct.get(0).getId());
          Assert.assertEquals(product.get(0).getNombreProducto(), dtoIntProduct.get(0).getNombreProducto());
          Assert.assertEquals(product.get(0).getDescripcionProducto(), dtoIntProduct.get(0).getDescripcionProducto());
          Assert.assertEquals(product.get(0).getCantidad(), dtoIntProduct.get(0).getCantidad());
          Assert.assertEquals(product.get(0).getFechaDeIngreso(), dtoIntProduct.get(0).getFechaDeIngreso());
          Assert.assertEquals(product.get(0).getUsuario().getId(), dtoIntProduct.get(0).getUsuario().getId());
          Assert.assertEquals(product.get(0).getUsuario().getNombre(), dtoIntProduct.get(0).getUsuario().getNombre());
          Assert.assertEquals(product.get(0).getUsuario().getEdad(), dtoIntProduct.get(0).getUsuario().getEdad());
          Assert.assertEquals(product.get(0).getUsuario().getFechaDeIngresoACompañia(), dtoIntProduct.get(0).getUsuario().getFechaDeIngresoACompañia());
          Assert.assertEquals(product.get(0).getUsuario().getCargo().getId(), dtoIntProduct.get(0).getUsuario().getCargo().getId());
          Assert.assertEquals(product.get(0).getUsuario().getCargo().getDescripcion(), dtoIntProduct.get(0).getUsuario().getCargo().getDescripcion());
          Assert.assertEquals(product.get(0).getUsuarioActualizo().getId(), dtoIntProduct.get(0).getUsuarioActualizo().getId());
          Assert.assertEquals(product.get(0).getUsuarioActualizo().getNombre(), dtoIntProduct.get(0).getUsuarioActualizo().getNombre());
          Assert.assertEquals(product.get(0).getUsuarioActualizo().getEdad(), dtoIntProduct.get(0).getUsuarioActualizo().getEdad());
          Assert.assertEquals(product.get(0).getUsuarioActualizo().getFechaDeIngresoACompañia(), dtoIntProduct.get(0).getUsuarioActualizo().getFechaDeIngresoACompañia());
          Assert.assertEquals(product.get(0).getUsuarioActualizo().getCargo().getId(), dtoIntProduct.get(0).getUsuarioActualizo().getCargo().getId());
          Assert.assertEquals(product.get(0).getUsuarioActualizo().getCargo().getDescripcion(), dtoIntProduct.get(0).getUsuarioActualizo().getCargo().getDescripcion());
     }

     @Test
     public void testMapOutProduct(){
          DTOIntProducto dtoIntProduct = dummyMock.getOutProductDTO();
          Producto product = iServiceMapper.mapOutProduct(dtoIntProduct);
          Assert.assertNotNull(product);
          Assert.assertEquals(product.getId(), dtoIntProduct.getId());
          Assert.assertEquals(product.getNombreProducto(), dtoIntProduct.getNombreProducto());
          Assert.assertEquals(product.getDescripcionProducto(), dtoIntProduct.getDescripcionProducto());
          Assert.assertEquals(product.getCantidad(), dtoIntProduct.getCantidad());
          Assert.assertEquals(product.getFechaDeIngreso(), dtoIntProduct.getFechaDeIngreso());
          Assert.assertEquals(product.getUsuario().getId(), dtoIntProduct.getUsuario().getId());
          Assert.assertEquals(product.getUsuario().getNombre(), dtoIntProduct.getUsuario().getNombre());
          Assert.assertEquals(product.getUsuario().getEdad(), dtoIntProduct.getUsuario().getEdad());
          Assert.assertEquals(product.getUsuario().getFechaDeIngresoACompañia(), dtoIntProduct.getUsuario().getFechaDeIngresoACompañia());
          Assert.assertEquals(product.getUsuario().getCargo().getId(), dtoIntProduct.getUsuario().getCargo().getId());
          Assert.assertEquals(product.getUsuario().getCargo().getDescripcion(), dtoIntProduct.getUsuario().getCargo().getDescripcion());
          Assert.assertEquals(product.getUsuarioActualizo().getId(), dtoIntProduct.getUsuarioActualizo().getId());
          Assert.assertEquals(product.getUsuarioActualizo().getNombre(), dtoIntProduct.getUsuarioActualizo().getNombre());
          Assert.assertEquals(product.getUsuarioActualizo().getEdad(), dtoIntProduct.getUsuarioActualizo().getEdad());
          Assert.assertEquals(product.getUsuarioActualizo().getFechaDeIngresoACompañia(), dtoIntProduct.getUsuarioActualizo().getFechaDeIngresoACompañia());
          Assert.assertEquals(product.getUsuarioActualizo().getCargo().getId(), dtoIntProduct.getUsuarioActualizo().getCargo().getId());
          Assert.assertEquals(product.getUsuarioActualizo().getCargo().getDescripcion(), dtoIntProduct.getUsuarioActualizo().getCargo().getDescripcion());
     }

     @Test
     public void testMapIntPosition(){
          Cargo cargo = dummyMock.getMapInPosition();
          DTOIntCargo dtoIntCargo = iServiceMapper.mapInPosition(cargo);
          Assert.assertNotNull(dtoIntCargo);
          Assert.assertEquals(dtoIntCargo.getDescripcion(), cargo.getDescripcion());
     }

     @Test
     public void testMapInCreateUser(){
          Usuario usuario = dummyMock.getMapInUser();
          DTOIntUsuario dtoIntUsuario = iServiceMapper.mapIntUser(usuario);
          Assert.assertNotNull(dtoIntUsuario);
          Assert.assertEquals(dtoIntUsuario.getNombre(), usuario.getNombre());
          Assert.assertEquals(dtoIntUsuario.getEdad(), usuario.getEdad());
          Assert.assertEquals(dtoIntUsuario.getIdCargo(), usuario.getIdCargo());
          Assert.assertEquals(dtoIntUsuario.getFechaDeIngresoACompañia(), usuario.getFechaDeIngresoACompañia());
     }

     @Test
     public void testMapOutListUsers(){
          List<DTOIntUsuario> dtoIntUsuarioList = dummyMock.getListUsers();
          List<Usuario> usuarioList = iServiceMapper.mapOutUsuario(dtoIntUsuarioList);
          Assert.assertNotNull(usuarioList);
          Assert.assertEquals(usuarioList.get(0).getId(), dtoIntUsuarioList.get(0).getId());
          Assert.assertEquals(usuarioList.get(0).getNombre(), dtoIntUsuarioList.get(0).getNombre());
          Assert.assertEquals(usuarioList.get(0).getEdad(), dtoIntUsuarioList.get(0).getEdad());
          Assert.assertEquals(usuarioList.get(0).getFechaDeIngresoACompañia(), dtoIntUsuarioList.get(0).getFechaDeIngresoACompañia());
          Assert.assertEquals(usuarioList.get(0).getCargo().getId(), dtoIntUsuarioList.get(0).getCargo().getId());
          Assert.assertEquals(usuarioList.get(0).getCargo().getDescripcion(), dtoIntUsuarioList.get(0).getCargo().getDescripcion());
     }
}
