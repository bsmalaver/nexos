package com.prueba.nexos.service.impl;

import com.prueba.nexos.DummyMock;
import com.prueba.nexos.controller.Controller;
import com.prueba.nexos.repository.IRepositoryPosition;
import com.prueba.nexos.repository.IRepositoryProduct;
import com.prueba.nexos.repository.IRepositoryUser;
import com.prueba.nexos.service.dto.Producto;
import com.prueba.nexos.service.dto.Usuario;
import com.prueba.nexos.service.mapper.impl.ServiceMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ServiceImplTest {


     private DummyMock dummyMock;

     @Mock
     IRepositoryProduct iRepositoryProduct;

     @Mock
     IRepositoryUser iRepositoryUser;

     @Mock
     IRepositoryPosition iRepositoryPosition;

     @Mock
     ServiceMapper serviceMapper;

     @InjectMocks
     ServiceImpl serviceImpl;

     @Before
     public void setUp() throws Exception {
          MockitoAnnotations.initMocks(this);
          dummyMock = new DummyMock();
          serviceMapper = new ServiceMapper();
     }

     @Test
     public void createProduct() throws Exception{
          serviceImpl.createProduct(dummyMock.getCreateInProduct());
     }

     @Test
     public void testGetProduct() {
          Mockito.when(iRepositoryProduct.findAllByNombreProducto("XBOX")).thenReturn(dummyMock.getProduct());
          serviceImpl.getProduct("XBOX");
     }

     @Test
     public void testListAllProducts() {
          Mockito.when(iRepositoryProduct.findAll()).thenReturn(dummyMock.getOutProduct());
          List<Producto> response = serviceImpl.listProducts();
          Assert.assertNotNull(response);
     }

     @Test
     public void testUpdateProduct() {
          Mockito.when(iRepositoryProduct.findByNombreProducto("XBOX")).thenReturn(dummyMock.getCreateInProduct());
          serviceImpl.updateProduct(dummyMock.getCreateInProduct(), "XBOX");
     }

     @Test
     public void testDeleteProduct() {
          Mockito.when(iRepositoryProduct.findByNombreProducto("XBOX")).thenReturn(dummyMock.getDeleteProduct());
          serviceImpl.removeProduct("XBOX", "Brayan");
     }


     @Test
     public void testListUsers() {
          Mockito.when(iRepositoryUser.findAll()).thenReturn(dummyMock.getListUsers());
          List<Usuario> response = serviceImpl.listUsers();
          Assert.assertNotNull(response);
     }

     @Test
     public void testCreateUser() {
          serviceImpl.createUser(dummyMock.getCreateUser());
     }

     @Test
     public void testCreatePosition() {
          serviceImpl.createCargo(dummyMock.getCreatePosition());
     }


}
