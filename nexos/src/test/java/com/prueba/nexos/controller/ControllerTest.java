package com.prueba.nexos.controller;

import com.prueba.nexos.DummyMock;
import com.prueba.nexos.service.dto.Producto;
import com.prueba.nexos.service.dto.Usuario;
import com.prueba.nexos.service.impl.ServiceImpl;
import com.prueba.nexos.service.mapper.impl.ServiceMapper;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;

import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class ControllerTest {

     private DummyMock dummyMock;

     @Mock
     ServiceImpl serviceImpl;

     @Mock
     ServiceMapper serviceMapper;

     @InjectMocks
     Controller controller;

     @Before
     public void setUp() throws Exception{
          dummyMock = new DummyMock();
          serviceMapper = new ServiceMapper();
     }

     @Test
     public void testCreateProduct(){
          controller.createProduct(dummyMock.getInProduct());
     }

     @Test
     public void testListProducts(){
          Mockito.when(serviceImpl.listProducts()).thenReturn(dummyMock.getOutListProduct());
          controller.listProduct();
     }

     @Test
     public void testGetProduct(){
          Mockito.when(serviceImpl.getProduct("XBOX")).thenReturn(dummyMock.getInProduct());
          Producto producto = controller.getProduct("XBOX");
          Assert.assertNotNull(producto);
     }

     @Test
     public void testUpdateProduct(){
          controller.updateProduct(dummyMock.getInProduct(), "XBOX");
     }

     @Test
     public void testDeleteProduct(){
          controller.deleteProduct("XBOX", "Brayan");
     }

     @Test
     public void testCreateUser(){
          controller.createUser(dummyMock.getMapInUser());
     }

     @Test
     public void testListUser(){
          Mockito.when(serviceImpl.listUsers()).thenReturn(dummyMock.getOutListUsers());
          List<Usuario> usuarios = controller.listUsers();
          Assert.assertNotNull(usuarios);
     }

     @Test
     public void testCreatePosition(){
          controller.createCargo(dummyMock.getMapInPosition());
     }
}
