import { EventEmitter, Injectable, Output } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Usuarios } from '../dto/usuarios';
import { Producto } from '../dto/producto';

@Injectable({ 
  providedIn: 'root'
})
export class Services {

  

  private url = "http://localhost:8080";

  constructor(private http: HttpClient) { }

    get():Observable<Usuarios[]>{
    return this.http.get<Usuarios[]>(this.url + "/nexos/usuarios",{
      headers:{
        'Access-Control-Allow-Origin': '*',
        'Content-Type':'application/json'
      }
    })
  }

  getProduct(id:any):Observable<Producto>{
    return this.http.get<Producto>(this.url + "/nexos/product/"+id,{
      headers:{
        'Access-Control-Allow-Origin': '*',
        'Content-Type':'application/json'
      }
    })
  }

  listProducts():Observable<Producto[]>{
    return this.http.get<Producto[]>(this.url + "/nexos/products",{
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type':'application/json'
      }
    }
    )
  }

  createProduct(product:Producto):Observable<Producto>{
    return this.http.post<Producto>(this.url + "/nexos/product", product,{
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type':'application/json'
      }
    });
  }

  updateProduct(product:Producto, id: any):Observable<Producto>{
    return this.http.put<Producto>(this.url + "/nexos/product/"+id, product,{
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Content-Type':'application/json'
      }
    });
  }

  deleteProduct(id:any):Observable<Producto>{
    return this.http.delete<Producto>(this.url + "/nexos/product/"+id,{
      headers:{
        'Access-Control-Allow-Origin': '*',
        'Content-Type':'application/json'
      }
    })
  }

}
