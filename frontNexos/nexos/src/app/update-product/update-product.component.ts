import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Producto } from '../dto/producto';
import { Usuarios } from '../dto/usuarios';
import { Services } from '../services/services';

@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {

  users: Usuarios[] = [];
  product: Producto = new Producto();
  tittle="Registrar nuevo producto";

  constructor(private services:Services, private router:Router) { }

  ngOnInit(): void {
    this.services.get().subscribe(r=>this.users=r);
  }

  updateProduct():void{
    this.services.updateProduct(this.product, this.product.id).subscribe(
      res=>this.router.navigate(['/productos'])
    )
  }
}
