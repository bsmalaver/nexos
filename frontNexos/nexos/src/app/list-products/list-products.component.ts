import { Component, Input, OnInit } from '@angular/core';
import { Services } from '../services/services';
import { Producto } from '../dto/producto';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css']
})
export class ListProductsComponent implements OnInit {

  constructor(private services:Services) { }
  public listProducts : Producto[] = [];
  ngOnInit(): void {
    this.services.listProducts().subscribe(r=>this.listProducts=r)
  }

}
