import { Usuarios } from "./usuarios";

export class Producto{
    id: any;
    nombreProducto: any;
    descripcionProducto: any;
    cantidad: any;
    fechaDeIngreso: any;
    idUsuario: any;
    idUsuarioActualizo: any;
    usuario: Usuarios = new Usuarios;
    usuarioActualizo?: Usuarios ;
    
}