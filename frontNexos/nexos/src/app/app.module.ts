import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListUsersComponent } from './list-users/list-users.component';
import { HeaderComponent } from './header/header.component';
import { TableUsersComponent } from './table-users/table-users.component';
import { CreateProductComponent } from './create-product/create-product.component';
import { UpdateProductComponent } from './update-product/update-product.component';
import { HttpClientModule } from '@angular/common/http';
import { ListProductsComponent } from './list-products/list-products.component';

import {Routes} from '@angular/router';
import { FormsModule } from '@angular/forms';

const routes: Routes=[
  {path:'', redirectTo: '/productos', pathMatch:'full'},
  {path:'productos', component:ListProductsComponent},
  {path:'productos/new', component:CreateProductComponent},
  {path:'productos/update', component:UpdateProductComponent}
];


@NgModule({
  declarations: [
    AppComponent,
    ListUsersComponent,
    HeaderComponent,
    TableUsersComponent,
    CreateProductComponent,
    UpdateProductComponent,
    ListProductsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
