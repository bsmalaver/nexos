import { Component, OnInit } from '@angular/core';
import { AppComponent } from '../app.component';
import { ListUsersComponent } from '../list-users/list-users.component';
import { Services } from '../services/services';
import { Usuarios } from '../dto/usuarios';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    
  }

}
