import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Producto } from '../dto/producto';
import { Usuarios } from '../dto/usuarios';
import { Services } from '../services/services';

@Component({
  selector: 'app-create-product',
  templateUrl: './create-product.component.html',
  styleUrls: ['./create-product.component.css']
})
export class CreateProductComponent implements OnInit {

  users: Usuarios[] = [];
  product: Producto = new Producto();
  tittle="Registrar nuevo producto";

  constructor(private services:Services, private router:Router) { }

  ngOnInit(): void {
    this.services.get().subscribe(r=>this.users=r);
  }

  createProduct():void{
    this.services.createProduct(this.product).subscribe(
      res=>this.router.navigate(['/productos'])
    )
  }

}
